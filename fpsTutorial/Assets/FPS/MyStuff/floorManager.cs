﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class floorManager : MonoBehaviour
{
    public  GameObject[] endFloor;
    public  GameObject[] invisFloor;

    public static Hashtable EndFloors = new Hashtable();
    public static Hashtable InvisFloors = new Hashtable();

    invisHandler invisH;

    // Start is called before the first frame update
    void Start()
    {
        for(int i = 0; i < 128; i++)
        {
            EndFloors.Add(i, endFloor[i]);        
        }

        for (int i = 0; i < 104; i++)
        {
            InvisFloors.Add(invisFloor[i], i);          
        }

    }

    // Update is called once per frame
    /*void Update()
    {
    }*/
}
