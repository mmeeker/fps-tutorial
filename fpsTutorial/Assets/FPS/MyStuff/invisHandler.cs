﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class invisHandler : MonoBehaviour
{
    public bool invis;

    // Start is called before the first frame update
    void Start()
    {
        if (this.name == "InvisFloor")
        {
            this.GetComponent<MeshCollider>().enabled = false;
            invis = true;
        }
        else
        {
            this.GetComponent<MeshCollider>().enabled = true;
            invis = false;
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (this.invis)
            this.GetComponent<MeshCollider>().enabled = false;
        else 
            this.GetComponent<MeshCollider>().enabled = true;

    }

    public void reverseInvis()
    {
        if (invis)
            this.invis = false;
        else
            this.invis = true;

    }
}
