﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class startCommentary : MonoBehaviour
{
    AudioSource m_MyAudioSource;
    public bool end;
    public bool played;

    // Start is called before the first frame update
    void Start()
    {
        m_MyAudioSource = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {
        if(played && end && !m_MyAudioSource.isPlaying)
        {
            for (int i = 0; i < 128; i++)
            {
                GameObject endFloor = (GameObject)floorManager.EndFloors[i];

                if (endFloor.GetComponent<invisHandler>().invis == true)
                {
                    endFloor.GetComponent<Renderer>().material.color = Color.grey;

                    endFloor.GetComponent<invisHandler>().reverseInvis();
                }

            }
            end = false;
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (!played)
        {
            m_MyAudioSource.Play();
            played = true;
        }
    }

}

